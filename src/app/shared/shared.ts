import { Map } from "./../model/map-model";
import { Player } from "./../model/player-model";
import { Injectable } from "@angular/core";

@Injectable()
export class Shared {
  playerCanMove: boolean;
  constructor() {}

  /* Handling player action depending on action character */
  playerAction(moveChar: string, player, map: Map) {
    switch (moveChar) {
      case "A":
        this.updatePlayerPosition(player, map);
        break;
      case "G":
      case "D":
        this.updatePlayerDirection(moveChar, player);
        break;
    }
  }

  /* Update player and map treasures */
  updateTreasures(move: string, player: Player, map: Map) {
    /* Avoid updating when player stay on same case */
    if (move == "A" && this.playerCanMove) {
      for (let treasure of map.treasureArray.treasure) {
        if (
          treasure.numberLeft > 0 &&
          player.xPosition == treasure.xPosition &&
          player.yPosition == treasure.yPosition
        ) {
          this.addUserTreasure(player);
          this.removeMapTreasure(treasure);
        }
      }
    }
  }

  /* Update player orientation depending on his action and his current orientation */
  private updatePlayerDirection(moveChar: string, player: Player) {
    switch (player.geoOrientation) {
      /* Current player orientation = NORTH */
      case "N": {
        switch (moveChar) {
          case "G": {
            player.geoOrientation = "O";
            break;
          }
          case "D": {
            player.geoOrientation = "E";
            break;
          }
        }
        break;
      }
      /* Current player orientation = SOUTH */
      case "S": {
        switch (moveChar) {
          case "G": {
            player.geoOrientation = "E";
            break;
          }
          case "D": {
            player.geoOrientation = "O";
            break;
          }
        }
        break;
      }
      /* Current player orientation = EAST */
      case "E": {
        switch (moveChar) {
          case "G": {
            player.geoOrientation = "N";
            break;
          }
          case "D": {
            player.geoOrientation = "S";
            break;
          }
        }
        break;
      }
      /* Current player orientation = WEST */
      case "O": {
        switch (moveChar) {
          case "G": {
            player.geoOrientation = "S";
            break;
          }
          case "D": {
            player.geoOrientation = "N";
            break;
          }
        }
        break;
      }
    }
  }

  /* Update player position depending on his orientation and on map */
  private updatePlayerPosition(player: Player, map: Map) {
    var playerNextPositionX = player.xPosition;
    var playerNextPositionY = player.yPosition;
    switch (player.geoOrientation) {
      case "N":
        playerNextPositionY = player.yPosition - 1;
        break;
      case "S":
        playerNextPositionY = player.yPosition + 1;
        break;
      case "E":
        playerNextPositionX = player.xPosition + 1;
        break;
      case "O":
        playerNextPositionX = player.xPosition - 1;
        break;
    }

    // Update the player position only if there is no mountain
    if (this.doPlayerCanMove(playerNextPositionX, playerNextPositionY, map)) {
      player.xPosition = playerNextPositionX;
      player.yPosition = playerNextPositionY;
    }
  }

  /* Check if player can move depending on his position, his action, map limit, and mountains */
  private doPlayerCanMove(playerNextPositionX, playerNextPositionY, map: Map) {
    /* Is false if there is a mountain */
    for (let mountain of map.mountainArray.moutain) {
      if (
        playerNextPositionX == mountain.xPosition &&
        playerNextPositionY == mountain.yPosition
      ) {
        this.playerCanMove = false;
        return false;
      }
    }
    /* Is false if player is at the limit of the map */
    if (
      playerNextPositionX < 0 ||
      playerNextPositionX > map.width ||
      playerNextPositionY < 0 ||
      playerNextPositionY > map.height
    ) {
      this.playerCanMove = false;
      return false;
    } else {
      this.playerCanMove = true;
      return true;
    }
  }

  private addUserTreasure(player: Player) {
    player.nbOfTreasures++;
  }

  private removeMapTreasure(treasure) {
    treasure.numberLeft--;
  }
}
