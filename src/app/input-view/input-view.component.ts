import { Player } from "./../model/player-model";
import { Map } from "./../model/map-model";
import { AllDatas } from "./../model/all-datas.model";
import { DataService } from "./../services/datas.service";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-input-view",
  templateUrl: "./input-view.component.html",
  styleUrls: ["./input-view.component.css"]
})
export class InputViewComponent implements OnInit {
  private allDatas: AllDatas;

  constructor(private dataService: DataService) {}

  ngOnInit() {
    /* To be replaced by input parsing */
    this.allDatas = {
      playerArray: [
        {
          name: "Lara",
          xPosition: 1,
          yPosition: 1,
          geoOrientation: "S",
          movementSeq: ["A", "A", "D", "A", "D", "A", "G", "G", "A"],
          nbOfTreasures: 0
        }
      ],
      mapArray: {
        width: 3,
        height: 4,
        mountainArray: {
          moutain: [
            {
              xPosition: 1,
              yPosition: 0
            },
            {
              xPosition: 2,
              yPosition: 1
            }
          ]
        },
        treasureArray: {
          treasure: [
            {
              xPosition: 0,
              yPosition: 3,
              numberLeft: 2
            },
            {
              xPosition: 1,
              yPosition: 3,
              numberLeft: 3
            }
          ]
        }
      }
    };
    this.sendDatas();
  }

  sendDatas() {
    this.dataService.playerDatas(this.allDatas);
  }
}
