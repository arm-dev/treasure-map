import { DataService } from "./services/datas.service";
import { Shared } from "./shared/shared";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";

import { AppComponent } from "./app.component";
import { InputViewComponent } from "./input-view/input-view.component";
import { OutputViewComponent } from "./output-view/output-view.component";

@NgModule({
  declarations: [AppComponent, InputViewComponent, OutputViewComponent],
  imports: [BrowserModule, FormsModule],
  providers: [DataService, Shared],
  bootstrap: [AppComponent]
})
export class AppModule {}
