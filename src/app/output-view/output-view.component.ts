import { DataService } from "./../services/datas.service";
import { AllDatas } from "./../model/all-datas.model";
import { Shared } from "./../shared/shared";
import { Player } from "./../model/player-model";
import { Map } from "./../model/map-model";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-output-view",
  templateUrl: "./output-view.component.html",
  styleUrls: ["./output-view.component.css"]
})
export class OutputViewComponent implements OnInit {
  private allDatas: AllDatas;
  private mapDatas: Map;

  constructor(private datasService: DataService, private Shared: Shared) {}

  ngOnInit() {
    this.datasService.currentData.subscribe(response => {
      this.allDatas = response;
      var mapResponse = this.allDatas.mapArray;

      /* Get datas depending on each Player */
      for (let player of this.allDatas.playerArray) {
        /* Repeat till there is no more action left for player */
        for (let move of player.movementSeq) {
          this.result(move, player, mapResponse);
          this.mapDatas = mapResponse;
        }
      }
    });
  }

  private result(move: string, player: Player, map: Map) {
    /* Update map and player position depending on player action */
    this.Shared.playerAction(move, player, map);
    /* Update map and player treasure depending on player action */
    this.Shared.updateTreasures(move, player, map);
  }
}
