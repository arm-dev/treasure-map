export class Map {
  width: number;
  height: number;
  mountainArray: {
    moutain: [
      {
        xPosition: number;
        yPosition: number;
      }
    ];
  };
  treasureArray: {
    treasure: [
      {
        xPosition: number;
        yPosition: number;
        numberLeft: number;
      }
    ];
  };
}
