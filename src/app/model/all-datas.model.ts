import { Map } from "./map-model";
import { Player } from "./player-model";
export class AllDatas {
  playerArray: [Player];
  mapArray: Map;
}
