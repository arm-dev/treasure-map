export class Player {
  name: string;
  xPosition: number;
  yPosition: number;
  geoOrientation: string;
  movementSeq: Array<string>;
  nbOfTreasures: number;
}
