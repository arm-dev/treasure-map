import { AllDatas } from "./../model/all-datas.model";
import { Map } from "./../model/map-model";
import { Player } from "./../model/player-model";
import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs/BehaviorSubject";

@Injectable()
export class DataService {
  private datas: AllDatas;
  private dataSource = new BehaviorSubject(this.datas);

  currentData = this.dataSource.asObservable();
  constructor() {}

  playerDatas(datas) {
    this.dataSource.next(datas);
  }
}
